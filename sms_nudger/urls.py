from django.urls import path
from .views import (
    # PostListView,
    # PostDetailView,
    # PostCreateView,
    # PostUpdateView,
    # PostDeleteView,
    NudgeListView,
    NudgeCreateView,
    NudgeDeleteView
)
from . import views

urlpatterns = [
    path('', views.home, name='sms-nudger-home'),
    path('about/', views.about, name='sms-nudger-about'),
    path('status/', views.status, name='sms-nudger-status'),
    path('nudges/', NudgeListView.as_view(), name='nudge-list'),
    path('nudge/new/', NudgeCreateView.as_view(), name='nudge-create'),
    path('nudge/<int:pk>/delete', NudgeDeleteView.as_view(), name='nudge-delete')
    # path('posts/', PostListView.as_view(), name='sms-nudger-post-home'),
    # path('post/<int:pk>/', PostDetailView.as_view(), name='post-detail'),
    # path('post/new/', PostCreateView.as_view(), name='post-create'),
    # path('post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
    # path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='post-delete')
]
