from twilio.rest import Client

import environ
import os

# initialise environment variables
env = environ.Env()
environ.Env.read_env()


def send_sms_nudge(name, phone, body):
    # Credentials takens from .env
    account_sid = os.environ.get("ACCOUNT_SID")
    auth_token = os.environ.get("AUTH_TOKEN")

    # TODO: Need to make sure phone numbers are cleaned
    # TODO: May need to concatonate a + in front of phone numbers
    cleaned_phone = phone

    client = Client(account_sid, auth_token)

    message = client.messages.create(
        to=f"+{cleaned_phone}",
        from_=os.environ.get("FROM_PHONE"),
        body=f"{name}, {body}",
    )
    return message
