from django.contrib import admin
from .models import Student, Nudge, Message, Post


# Register your models here.


# class StudentAdmin(admin.ModelAdmin):
#     list_display = ('first_name',
#                     'last_name',
#                     'email',
#                     'txt_ok_ind',
#                     'email_ok_ind',
#                     'dim_pgm_of_study_cur',
#                     'birth_date',
#                     'overall_credits',
#                     'overall_gpa',
#                     'canvas_user_id'
#                     )


class StudentAdmin(admin.ModelAdmin):
    list_display = ('first_name',
                    'last_name',
                    'txt_ok_ind',
                    'email_ok_ind',
                    )


class MessageAdmin(admin.ModelAdmin):
    list_display = ('subject',
                    'body'
                    )
    search_fields = ['subject',
                     'body'
                     ]


class NudgeAdmin(admin.ModelAdmin):
    list_display = ('scheduled_datetime',
                    'sent_datetime',
                    # 'student',  # Can't because of ManyToMany Relationship
                    'nudge_sender',
                    'nudge_type',
                    'message'
                    )
    search_fields = ['scheduled_datetime',
                     'sent_datetime',
                     'student__first_name',
                     'student__last_name',
                     'nudge_sender',
                     'nudge_type',
                     'message__subject',
                     'message__body'
                     ]


admin.site.register(Post)
admin.site.register(Student, StudentAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Nudge, NudgeAdmin)
