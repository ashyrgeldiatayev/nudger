# from django.contrib.auth.models import User  # noqa
from django.contrib import messages
from django.http import HttpResponse
from django.views.generic.edit import FormView
from django.shortcuts import redirect, render

from .forms import GenerateRandomUserForm
from .tasks import create_random_user_accounts


from django.contrib.auth.models import User
from sms_nudger.models import Student, Nudge, Message
from .models import Post


from django import forms
from django.forms import DateInput
from django.forms import DateTimeInput
from django.forms import ModelChoiceField
from django.contrib.admin.widgets import FilteredSelectMultiple
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

# from django.utils import timezone
from datetime import datetime
import pytz

from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)

import django_filters
from django_filters.views import FilterView

import os
from django.urls import path, include

from django.contrib import admin
# Class-based View (List View)


# class PostListView(ListView):
#     model = Post
#     # Usually looks for a specifically-name file
#     # so we need to redirect it
#     template_name = 'sms_nudger/post-home.html'  # <app>/<model>_<viewtype>.html
#     # Set "ObjectList" to "Post"
#     context_object_name = 'posts'
#     ordering = ['-date_posted']

# # Class-based View (Detailed View)


# class PostDetailView(DetailView):
#     model = Post


# class PostCreateView(LoginRequiredMixin, CreateView):
#     model = Post
#     fields = ['title', 'content']

#     def form_valid(self, form):
#         form.instance.author = self.request.user
#         return super().form_valid(form)


# class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
#     model = Post
#     fields = ['title', 'content']

#     def form_valid(self, form):
#         form.instance.author = self.request.user
#         return super().form_valid(form)

#     def test_func(self):
#         post = self.get_object()
#         if self.request.user == post.author:
#             return True
#         return False


# class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
#     model = Post
#     success_url = '/'

#     def test_func(self):
#         post = self.get_object()
#         if self.request.user == post.author:
#             return True
#         return False


def home(request):
    return render(request, 'sms_nudger/home.html')


def status(request):
    context = dict(
        nudges=[
            dict(
                name='register for classes',
                description='1 day before registration closes remind student to register for clases',
                trigger='2022-01-05',
                kind='scheduled',
            ),
            dict(
                name='attend graduation',
                description='1 day before registration closes remind student about graduation',
                trigger='2026-06-01',
                kind='scheduled',
            ),
        ])
    return render(request, 'sms_nudger/status.html', context=context)


def about(request):
    return render(request, 'sms_nudger/about.html')


class NudgeFilter(django_filters.FilterSet):
    CHOICES = (
        ('ascending', 'Ascending'),
        ('descending', 'Descending')
    )

    order = django_filters.ChoiceFilter(label='Ordering', choices=CHOICES, method='filter_by_order')

    class Meta:
        model = Nudge
        fields = [
            'student'
        ]

    def filter_by_order(self, queryset, name, value):
        expression = 'scheduled_datetime' if value == 'ascending' else '-scheduled_datetime'
        return queryset.order_by(expression)


class NudgeListView(ListView):
    model = Nudge
    template_name = 'sms_nudger/nudges.html'
    context_object_name = 'nudges'
    # ordering = ['-scheduled_datetime']
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = NudgeFilter(self.request.GET, queryset=self.get_queryset())
        return context


class NudgeDeleteView(LoginRequiredMixin, DeleteView):
    model = Nudge
    success_url = '/nudges'


# Works broadly but doesn't have time
class MyDateInput(forms.widgets.DateInput):
    input_type = 'date'


# Limited support but includes both date and time
class MyDateTimeInput(forms.widgets.DateInput):
    input_type = 'datetime-local'


# class StudentAdmin(admin.ModelAdmin):
#     filter_horizontal = ('students',)

class MessageModelChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        length = int(len(obj.body) / 2)
        print(length)
        preview = obj.body[slice(0, length)]
        return f"{obj.subject} - Preview: {preview}..."

class NudgeForm(forms.ModelForm):

    # This adds a checkbox selection for the field
    # student = forms.ModelMultipleChoiceField(
    #     widget=forms.CheckboxSelectMultiple(),
    #     queryset=Student.objects.all()
    # )

    # Might make two tables (all and selected), but not working
    # student = forms.ModelMultipleChoiceField(
    #     queryset=Student.objects.all(),
    #     label='Select Students',
    #     required=False,
    #     widget=FilteredSelectMultiple(
    #         'Students',
    #         False
    #     )
    # )

    # Replace "Message" with "forms." for subject only
    message = MessageModelChoiceField(
        widget=forms.RadioSelect(),
        queryset=Message.objects.all(),
    )

    class Media:
        extend = False
        css = {
            'all': [
                'admin/css/widgets.css'
            ]
        }
        js = (
            'js/django_global.js',
            'admin/js/jquery.init.js',
            'admin/js/core.js',
            'admin/js/prepopulate_init.js',
            'admin/js/prepopulate.js',
            'admin/js/SelectBox.js',
            'admin/js/SelectFilter2.js',
            'admin/js/admin/RelatedObjectLookups.js',
        )

    class Meta:
        model = Nudge
        fields = [
            'nudge_type',
            'scheduled_datetime',
            # 'sent_datetime',
            'student',
            'nudge_sender',
            'message'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(
            Row(
                Column('nudge_type', 'scheduled_datetime', css_class='form-group col-md-6 mb-0'),
                Column('nudge_sender', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('student', css_class='form-group col-md-6 mb-0'),
                css_class='form-row'
            ),
            Row(
                Column('message', css_class='form-group col-md-10 mb-0'),
                css_class='form-row'
            ),
            Submit('submit', 'Create nudge')
        )

    def initial_date():

        # Set the right timezone for the default form value
        MT = pytz.timezone('US/Mountain')

        # Sets the default form values to US/Mountain time
        year = datetime.now(MT).year
        month = datetime.now(MT).month
        day = datetime.now(MT).day
        hour = datetime.now(MT).hour
        minute = datetime.now(MT).minute

        initial_value = datetime(year, month, day, hour, minute).isoformat()

        return initial_value

    # Instantiates a widget and default datetime in the form
    scheduled_datetime = forms.DateTimeField(initial=initial_date, widget=MyDateTimeInput())
    # scheduled_datetime = forms.DateField(widget=MyDateInput())


# class NudgeAdmin(admin.ModelAdmin):
#     form = NudgeAdminForm


class NudgeCreateView(LoginRequiredMixin, CreateView):
    model = Nudge
    form_class = NudgeForm

    # TODO: Need to add a place to tell who the sender is

    def get_initial(self):
        return {
            'nudge_sender': 'TEACHER NAME PLACEHOLDER2'
        }

    success_url = '/'

    def form_valid(self, form):
        return super().form_valid(form)
