from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='convoscript-home'),
    path('about/', views.about, name='convoscript-about'),
    path('check/', views.about, name='convoscript_check'),
    path('export/', views.export, name='convoscript_export'),
    path('populate_form/', views.populate_form, name='convoscript_populateform')
]
