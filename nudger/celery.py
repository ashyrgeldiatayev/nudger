from __future__ import absolute_import, unicode_literals
import os
from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nudger.settings")

celery_app = Celery("nudger")

celery_app.config_from_object("django.conf:settings", namespace="CELERY")

celery_app.conf.timezone = "US/Mountain"
# celery_app.conf.timezone = 'UTC'

celery_app.autodiscover_tasks()


@celery_app.task(bind=True)
def debug_task(self):
    print("Request: {0!r}".format(self.request))


# celery_app.conf.beat_schedule = {
#     'add_10s': {
#         'task': 'chat_async.tasks.addition_test_task',
#         'schedule': 30.0,
#         'args': (10, 10),
#         'options': {
#             'expires': 15.0
#         }
#     }
# }
