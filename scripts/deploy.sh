#!/bin/sh

echo "Running deploy.sh for $DEPLOY_IP_ADDRESS == $1"

rsync -e "ssh -o StrictHostKeyChecking=no" -raz --progress ./ "root@${PRODUCTION_IP_ADDRESS}:nudger/"

ssh -o StrictHostKeyChecking=no root@$DEPLOY_IP_ADDRESS << 'ENDSSH'
  pwd

  cd nudger
  sudo systemctl restart gunicorn.service

ENDSSH


echo "Finished deploy.sh"
