#!/usr/bin/env bash

python manage.py shell <<EOF

print('Creating a superuser')

DJANGO_SUPERUSER_USERNAME = "$1"
DJANGO_SUPERUSER_PASSWORD = "$2"


from django.contrib.auth.models import User

User.objects.create_superuser(
    username = DJANGO_SUPERUSER_USERNAME,
    password = DJANGO_SUPERUSER_PASSWORD
)

print(f"len(password): len(DJANGO_SUPERUSER_PASSWORD)")

exit()

EOF

echo "Finished $0 for superusername $1"