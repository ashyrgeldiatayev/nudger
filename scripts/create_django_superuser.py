#!/usr/bin/env bash

# Create a superuser
from dotenv import load_dotenv, dotenv_values

load_dotenv()
ENV = dotenv_values()


from django.contrib.auth.models import User

User.objects.create_superuser(
    username='$1',
    password='$2'
)
