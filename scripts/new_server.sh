#!/bin/bash

# setup a new server

sudo apt-get update


pip install --upgrade pip
pip install -r requirements.txt

# curl -OL https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
# bash Miniconda3-latest-Linux-x86_64.sh
# conda init bash
# ~/miniconda3/bin/conda init bash

# conda create -y -n nudger 'python=3.8.8'
# conda activate nudger
# conda env update -n nudger -f environment.yml


curl -OL https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
~/miniconda3/bin/conda init bash
source ~/.bashrc
which conda
cd ~/nudger
conda create -n nudgerenv python==3.8.8
conda activate nudgerenv
# conda env export -n nudgerenv >> environment.yml
# nano environment.yml
conda env update -n nudger -f environment.yml
python manage.py test
