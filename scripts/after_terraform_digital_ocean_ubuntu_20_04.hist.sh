# nudger provisioning before gitlab-ci.yml
#
# STAGING_IP=192.241.189.170
# PRODUCTION_IP=159.223.223.144
FQDM=nudger.qary.ai
FQDM=$1

apt install nginx gunicorn
cp nudger/etc/gunicorn.service /etc/systemd/system/gunicorn.serice
cp nudger/etc/nudger_gunicorn.nginx /etc/systemd/system/
sudo service gunicorn restart
ls /etc/systemd/system/
ExecStart=gunicorn --bind unix:/root/nudger/run/gunicorn.sock nudger.wsgi:application
gunicorn --help

cd nudger/

# FIXME!!!:
chmod -R 755 /root/

journalctl -xe
# root@nudger-production:~# journalctl -xe
# No journal files were found.
# ~
# ~
# ~
# ~


ls /var/log/nginx
systemctl restart gunicorn.service nginx.service
sudo systemctl daemon-reload
sudo systemctl restart nginx
sudo systemctl restart gunicorn.service
systemctl restart gunicorn.service nginx.service
chmod -R ugoa+rwx /root/nudger/
systemctl restart gunicorn.service nginx.service
ls -al
ufw status
python manage.py test
python3 manage.py test
systemctl status gunicorn.service nginx.service
nano /etc/systemd/system/gunicorn.service
ls
cd run
ls
touch gunicorn.sock
systemctl status gunicorn.service nginx.service
systemctl restart gunicorn.service nginx.service
systemctl status gunicorn.service nginx.service
ls
nano gunicorn.sock 
rm gunicorn.sock 
nano /etc/systemd/system/gunicorn.service
ExecStart=gunicorn --bind unix:/root/nudger/run/gunicorn.sock nudger.wsgi:application
ls /root/
nano /etc/systemd/system/gunicorn.service
systemctl restart gunicorn.service nginx.service
systemctl daemon-reload
systemctl restart gunicorn.service nginx.service
ssh-keyscan
ssh-keyscan 192.241.189.170
curl iconfig.me
curl ifconfig.me
exit
ls
ls /tmp/
ls
touch i.txt
ls
cp i.txt /tmp/
ls /tmp/
rm /tmp/i.txt 
ls /tmp/
ls
rm i.txt 
ls
ls /tmp/
ls i
cd /tmp/
ls i
ls 
sudo rm -r README.md data docs environment.yml etc logs manage.py nudger readme-windows.md requirements.txt run scripts sms_nudger static terraform_digitalocean users
ls 
exit
cd nudger
ls -al
cat .env
exit
sudo apt-get install -y certbot python3-certbot-nginx
ls /etc/nginx/sites-available/
ls /etc/nginx/sites-enabled/nudger_gunicorn.nginx 
nano /etc/nginx/sites-enabled/nudger_gunicorn.nginx 
sudo systemctl daemon-reload
sudo systemctl restart nginx
sudo nginx -t
sudo ufw status
sudo certbot --nginx -d $FQDN

#######################################################################################
# AFTER PROVISIONINING AND SUCCESSFULLY DEPLOYED
# nudger.chat.ai (STAGING_IP=192.241.189.170)
cd /root/snap/
ls -hal
# root@nudger-staging:~/snap# ls -hal
# total 12K
# drwxr-xr-x 3 root root 4.0K Mar 17 19:12 .
# drwxr-xr-x 7 root root 4.0K May 17 19:01 ..
# drwxr-xr-x 5 root root 4.0K Mar 24 20:30 lxd

cd /root/
ls -hal
# root@nudger-staging:~# ls -hal
# total 48K
# drwxr-xr-x  7 root root 4.0K May 17 19:13 .
# drwxr-xr-x 19 root root 4.0K Apr 28 23:35 ..
# -rw-------  1 root root   60 May 17 19:13 .Xauthority
# -rw-------  1 root root 2.1K May 17 19:05 .bash_history
# -rwxr-xr-x  1 root root 3.1K Dec  5  2019 .bashrc
# drwxr-xr-x  3 root root 4.0K Mar 17 19:42 .cache
# -rwxr-xr-x  1 root root    0 Apr 28 23:35 .cloud-locale-test.skip
# drwxr-xr-x  3 root root 4.0K Mar 17 19:39 .local
# -rwxr-xr-x  1 root root  161 Dec  5  2019 .profile
# drwxr-xr-x  2 root root 4.0K Mar 17 19:12 .ssh
# -rwxr-xr-x  1 root root  185 May 17 19:20 .wget-hsts
# drwxrwxrwx 17 root root 4.0K May 17 05:24 nudger
# drwxr-xr-x  3 root root 4.0K Mar 17 19:12 snap

systemctl status gunicorn.service nginx.service
# root@nudger-staging:~# systemctl status gunicorn.service gunicorn.socket nginx.service
# Unit gunicorn.socket could not be found.
# root@nudger-staging:~# systemctl status gunicorn.service
# ● gunicorn.service - gunicorn daemon
#      Loaded: loaded (/etc/systemd/system/gunicorn.service; enabled; vendor pres>
#      Active: active (running) since Tue 2022-05-17 05:25:11 UTC; 13h ago
#    Main PID: 103097 (gunicorn: maste)
#       Tasks: 2 (limit: 1131)
#      Memory: 67.3M
#      CGroup: /system.slice/gunicorn.service
#              ├─103097 gunicorn: master [nudger.wsgi:application]
#              └─103099 gunicorn: worker [nudger.wsgi:application]
#
# May 17 16:39:06 nudger-staging gunicorn[103099]: Forbidden (CSRF cookie not set>
# May 17 17:02:44 nudger-staging gunicorn[103099]: Forbidden (CSRF cookie not set>
# May 17 17:20:41 nudger-staging gunicorn[103099]: Not Found: /index.php
# May 17 17:29:00 nudger-staging gunicorn[103099]: Forbidden (CSRF cookie not set>
# May 17 17:36:59 nudger-staging gunicorn[103099]: Not Found: /wp-login.php
# May 17 17:57:42 nudger-staging gunicorn[103099]: Forbidden (CSRF cookie not set>
# May 17 18:17:57 nudger-staging gunicorn[103099]: Not Found: /.git/config
# May 17 18:33:32 nudger-staging gunicorn[103099]: Forbidden (CSRF cookie not set>
# May 17 18:38:09 nudger-staging gunicorn[103099]: Forbidden (CSRF cookie not set>
# May 17 18:44:28 nudger-staging gunicorn[103099]: Not Found: /mUVogcXeWZy1TQrMKa>
#
# root@nudger-staging:~# systemctl status nginx.service
# ● nginx.service - A high performance web server and a reverse proxy server
#      Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset:>
#      Active: active (running) since Thu 2022-04-28 23:35:54 UTC; 2 weeks 4 days>
#        Docs: man:nginx(8)
#    Main PID: 7165 (nginx)
#       Tasks: 2 (limit: 1131)
#      Memory: 5.8M
#      CGroup: /system.slice/nginx.service
#              ├─7165 nginx: master process /usr/sbin/nginx -g daemon on; master_>
#              └─7167 nginx: worker process
#
# Apr 28 23:35:53 nudger-staging systemd[1]: Starting A high performance web serv>
# Apr 28 23:35:53 nudger-staging nginx[706]: nginx: [warn] server name "192.241.1>
# Apr 28 23:35:54 nudger-staging nginx[734]: nginx: [warn] server name "192.241.1>
# Apr 28 23:35:54 nudger-staging systemd[1]: Started A high performance web serve>
