# Celery Commands for MOIA

## Celery
Terminal 1: celery -A nudger.celery worker --loglevel = info

## Celery Beat
Terminal 2: celery -A nudger.celery beat --loglevel=info --scheduler django_celery_beat.schedulers:DatabaseScheduler

## Flower
Terminal 3: celery -A nudger flower --loglevel=info
