# Edge Cases

## Case 1
### Description
If two users use the same link, the conversation is duplicated.

### Steps to reproduce
  1. In Browser 1, open localhost:8000/testroom.
  2. Open another browser (Browser 2).
  3. In Browser 2, open localhost:8000/testroom.
  4. Switch to Browser 1.
  5. You will see the initial state created twice.

### Desired behavior
Chats conversations need to be consistent for individual users.  Individual users should not be put into the same room as another user.


## Case 2
### Description
A user wants to restart the conversation to choose a different user type.  There is no way to restart the conversation (other than to refresh the page).  This also applies to the language option.

### Steps to reproduce
  1. In a browser window, open localhost:8000/testroom.
  2. Choose "English"
  3. Choose "Ready"
  4. Choose "Yes"
  5. Choose "WSNYC Student"
  6. The user wanted to choose "WSNYC Volunteer + Educator."  
  7. As you continue through the conversation path, there is no way to switch roles or restart the conversation (other than refreshing).

### Desired behavior
A user should be able to trigger a restart for the conversation if there is no dialogue path to get where they want.

and/or

The user should be able to click a button to go back one state.
  

## Case 3
### Description 
When a user types in a response similar to the given options, the conversation will show an error that the response is not an option.

### Steps to reproduce
  #### Example 1
  1. In a browser window, open localhost:8000/testroom.
  2. Choose "English"
  3. Type "Ready!"
  4. An error message says "Response is not an option"

  #### Example 2
  1. In a browser window, open localhost:8000/testroom.
  2. Choose "Eng"
  3. An error message says "Response is not an option"

### Desired behavior
It would be nice for the system to recognize typed input that is close to one of the given options.  For example, "eng" (typed input) is close to "English" (given option).


## Case 4
### Description
The user types input while the messages are being sent with a delay.

### Steps to reproduce
  1. In a browser window, open localhost:8000/testroom.
  2. While the messages are loading, type "English."
  3. Immediately type "Ready"
  4. Immediately type "chatbot"
  5. Wait for the bot to progress.
  6. The chatbot should stop with an error message "Response is not an option."  The end state might vary depending on when the input stopped being recognized.

### Desired behavior
The input field should be disabled until the bot is ready for a response.

and/or

The bot should offer some way to reduce the delay (speed up the conversation).


## Case 5
### Description
When the user exits the chat, their conversation state is not saved.

### Steps to reproduce
  1. In a browser window, open localhost:8000/testroom.
  2. Choose "English"
  3. Close the window.
  4. In a browser window, open localhost:8000/testroom.
  5. The conversation will restart instead of resuming.

### Desired behavior
When the user restarts the chat using the same link, the conversation should resume if the link was opened within a certain amount of time (e.g., 1 day).


## Case 6
### Description
If the link for opening a window is too long or has unsupported characters, the chat will not connect properly.

### Steps to reproduce
  #### First Example
  1. In a browser window, open localhost:8000/testffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff.
  2.  The chat window will not work.
  
  #### Second Example
  1. In a browser window, open localhost:8000/test!.
  2.  The chat window will not work.

### Desired behavior
The chat link that initiates the connection to a channel should conform to appropriate URLs.

## Case 7

### Description
If the user scrolls through using tab, the button options do not highlight.

### Steps to reproduce
  1. In a browser window, open localhost:8000/test!.
  2. The input should be highlighted because the cursor is inside.
  3. Press tab until the button is selected (about 10 times).
  4. Press the spacebar.
  5. The form should submit the button content (e.g., "English")

### Desired behavior
The buttons should highlight regardless of how users are selecting them.


## Case 8

### Description
If the user scrolls through using tab, the button options do not highlight.

### Steps to reproduce
  1. In a browser window, open localhost:8000/test!.
  2. The input should be highlighted because the cursor is inside.
  3. Press tab until the button is selected (about 10 times).
  4. Press the enter.
  5. The form will not do anything.

### Desired behavior
Buttons selected through tabbing should be submitted when a user presses Enter.