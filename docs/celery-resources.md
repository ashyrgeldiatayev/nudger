# Resources to learn Celery 

## Official documentation

* [Canvas: Designing Work-flows](https://docs.celeryq.dev/en/latest/userguide/canvas.html#chains)

* [Periodic Tasks](https://docs.celeryq.dev/en/stable/userguide/periodic-tasks.html#using-custom-scheduler-classes)

## Blog posts

* [Groups, Chords, Chains and Callbacks](https://ask.github.io/celery/userguide/tasksets.html)

* [Chains, Groups and Chords in Celery](https://sayari3.com/articles/18-chains-groups-and-chords-in-celery/)

* [How to Use Celery and Django to Handle Periodic Tasks](https://www.nickmccullum.com/celery-django-periodic-tasks/)

* [Getting Started Scheduling Tasks with Celery](https://www.caktusgroup.com/blog/2014/06/23/scheduling-tasks-celery/)

* [How to Use Celery for Scheduling Tasks](https://www.caktusgroup.com/blog/2021/08/11/using-celery-scheduling-tasks/)

* [Using Celery With Django for Background Task Processing](https://code.tutsplus.com/tutorials/using-celery-with-django-for-background-task-processing--cms-28732)

* [Celery tutorials by Appliku](https://appliku.com/tag/celery)

* [Building a Task Queue with Celery and Redis](https://bilalozdemir.me/posts/python/task-queue-celery-redis/)

* [Asynchronous Tasks with Celery](https://overiq.com/django-1-11/asynchronous-tasks-with-celery/)

* [Dynamic Task Scheduling With Django-celery-beat](https://medium.com/swlh/dynamic-task-scheduling-with-django-celery-beat-f2591d52e15)

## Youtube playlists and talks

* [Follow your Celery tasks](https://www.youtube.com/watch?v=V6XRf457Es8&ab_channel=FOSDEM)

* Django real time Crypto application (channels, celery, and redis)

    * [Overview - Part 1](https://www.youtube.com/watch?v=Ib8UwwnPYsE&ab_channel=Pyplane)
    * [Part 2](https://www.youtube.com/watch?v=YQRg5Mrg1oY&t=31s&ab_channel=Pyplane)
    * [Part 3](https://www.youtube.com/watch?v=DhscKT1t8Vs&ab_channel=Pyplane)
    * [Part 4](https://www.youtube.com/watch?v=wfVwApNPcHs&ab_channel=Pyplane)

* [Real-time broadcasting API response app (jokes)](https://www.youtube.com/watch?v=AZNp1CfOjtE&list=WL&index=3&t=24s&ab_channel=RedEyedCoderClub)

* [Learn Django Celery by Very Academy](https://www.youtube.com/playlist?list=PLOLrQ9Pn6caz-6WpcBYxV84g9gwptoN20)

