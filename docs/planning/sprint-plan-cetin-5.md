- [ ] CR1: Change poly icon, create bundles and return the correct one, with Rochdi, Qary / Poly, change the parrot, qary icon"
- [ ] C1: Move object storage development to production, Information about static methods from Hobson.
- [ ] C1: Define nudger spaces on DigitalOcean
- [ ] C1: Remove buttons from quizbot. Use huggingface API to turn text to integers.
- [ ] R1: Disable error messages so bundles can send anything.
- [ ] C1: Define "else_" trigger for conversation. 

```python
# https://huggingface.co/spaces/TangibleAI/mathtext-nlu

import requests

requests.post(
url="https://Hobson-gradio-rest-api.hf.space/api/predict/", json={"data": ["one hundred forty-two"], "fn_index": 0}
).json() 
```
