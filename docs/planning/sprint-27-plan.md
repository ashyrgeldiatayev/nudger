# Sprint 27 (Sep 23 - Sep 30)

## Backend

* [x] R2-1.2: Ask some people for a feedback on old Poly on WeSpeakNYC and the new version on qary.ai
* [x] R1-1: Add a delay to each scheduled message that is dependent on the index variable i of the for loop
* [x] R1-1: Make sure all function calls within for loop are synchronous
* [x] R1-1: Add a minimum delay relative to the previous message of > than 0 for every message 
* [x] R2-1.5: Figure out an approach to fix the out-of-order bug
* [x] R0.2-0.2: Add the approach to the sprint plan 
* [x] R2-2: Create a simple example to learn/implement a Celery chain based on your approach
* [x] R2-0.5: Fix the bug you have in your simple Celery chain 
* [x] R1-1: Create another Celery chain that similates the state maintainance in Poly
* [x] R1-0.8: Share the examples with your team to get feedback 
* [x] R1-1: Add a delay to every task in the chain

## Frontend

* [x] R2-1: Move all widget elements to the front
* [x] R0.2-0.2: Create a new HTML file to make sure all the widget elements are in front
* [x] R0.5-0.5: Figure out how to maximize the widget size in small screens
* [x] R1-0.8: Remove the widget gate when it is clicked in small screens
* [x] R0.5-1: Maximize the width and height of the widget to fit the web page in small screens
* [x] R0.2-0.2: Deploy and test the new changes 
