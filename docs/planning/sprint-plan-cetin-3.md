# sprint-3-cetin

## Sprint 3

- [X] R: rochdi will build `localhost_bundle.js`
- [X] R: fix the broken deployment on qary.ai (vish pushed bugs)
- [X] test on localhost
- [X] update yaml file
- [X] load yaml into database locally
- [X] load yaml into database on render for Cetin's local account
- [X] load yaml into database on render for qary.ai on render

# - [ ] update instead of overwrite data in database

### Enhancements

- [X] additional correct strings for 1+1: two, 2
- [X] additional accepted strings for start: begin, ready

- [X] if yaml file fails to load try making state names unique by using "quizbot-" prefix on all state names


