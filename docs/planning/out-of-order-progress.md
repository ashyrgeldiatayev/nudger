# The journey to fix the out-of-order bug in Poly

## Our first approach to fix the bug

1. Create a task to get the current state object
2. Create a task to get the bot text messages
3. Create a task to get the bot triggers
4. Create a task to send user message (selected trigger)
5. Implement a chain where every task returns the state object
6. Make the last task of the chain updates the current state object
7. Call the chain within the channel consumers

## Learning and understanding Celery chains

### Our first Celery chain

I created a very simple chain to learn and understand how a chain is implemented in Celery. As known, chains work by passing the return value of the first task to the next task in the chain, so that the result of the previous task is passed as the first argument to the next task, and so on. 

Here is the chain with the tasks:

```python
from celery import chain, signature
from celery import shared_task



@shared_task(name = 'add')
def add(x, y):
    return x + y


@shared_task(name = 'substract')
def substract(x, y):
    return x - y


@shared_task(name = 'multiply')
def multiply(x, y):
    return x * y


@shared_task(name = 'divide')
def divide(x, y):
    return x / y


@shared_task(name = 'run-chain')
def run_chain(x, y):
    canvas = chain(
        add.s(x, y),
        substract.s(y),
        multiply.s(y),
        divide.s(y)
    ).apply_async()
    return canvas
```
This is where the chain is scheduled:

```python
app.conf.beat_schedule = {
    #Scheduler Name
    'execute-every-five-seconds': {
        # Task Name (Name Specified in Decorator)
        'task': 'run-chain',  
        # Schedule      
        'schedule': 5.0,
        # Function Arguments 
        'args': (2, 3) 
    }
}  
```
The output on Worker:

```terminal
[2022-09-28 08:21:19,956: INFO/MainProcess] Task run-chain[25fd3f7f-046b-4e4c-b9db-c3d8eb8d8138] received
[2022-09-28 08:21:19,962: INFO/ForkPoolWorker-4] Task run-chain[25fd3f7f-046b-4e4c-b9db-c3d8eb8d8138] succeeded in 0.005012649000491365s: <AsyncResult: 41df1c53-b086-4676-af56-945322615f17>
[2022-09-28 08:21:19,962: INFO/MainProcess] Task add[18d6e86e-6f3d-4288-867f-3d12ffc75db5] received
[2022-09-28 08:21:19,967: INFO/MainProcess] Task substract[a036c7e8-bf84-48fb-9055-95663f8500d4] received
[2022-09-28 08:21:19,968: INFO/ForkPoolWorker-4] Task add[18d6e86e-6f3d-4288-867f-3d12ffc75db5] succeeded in 0.0044167319992993725s: 5
[2022-09-28 08:21:19,973: INFO/ForkPoolWorker-2] Task substract[a036c7e8-bf84-48fb-9055-95663f8500d4] succeeded in 0.004064419001224451s: 2
[2022-09-28 08:21:19,973: INFO/MainProcess] Task multiply[6c0e97f0-d904-4ad9-bec4-d3dbe40d5b70] received
[2022-09-28 08:21:19,978: INFO/ForkPoolWorker-4] Task multiply[6c0e97f0-d904-4ad9-bec4-d3dbe40d5b70] succeeded in 0.0026280239999323385s: 6
[2022-09-28 08:21:19,978: INFO/MainProcess] Task divide[41df1c53-b086-4676-af56-945322615f17] received
[2022-09-28 08:21:19,980: INFO/ForkPoolWorker-4] Task divide[41df1c53-b086-4676-af56-945322615f17] succeeded in 0.0012471230002120137s: 2.0
```
### An example to simulate state maintainance in Poly

Then, I created another example to simulate the state maintainance in Poly:

```python
from celery import chain, signature
from celery import shared_task



@shared_task(name = 'state-name')
def get_state(state):
    return state


@shared_task(name = 'greeting')
def get_greeting(state, message):
    output = '=== ' + state + ': ' + message + ' ==='
    return output


@shared_task(name = 'get-identity')
def get_identity(state, identity):
    output = '=== ' + state + ': '+ identity + ' ==='
    return output


@shared_task(name = 'get-location')
def get_location(state, location):
    output = '=== ' + state + ': ' + location + ' ==='
    return output


@shared_task(name = 'run-chain')
def run_chain(state, message, identity, location):
    canvas = chain(
        get_state.s(state),
        get_greeting.s(message),
        get_identity.s(identity),
        get_location.s(location)
    ).apply_async()
    return canvas
```

This is where the chain is scheduled:

```python
app.conf.beat_schedule = {
    #Scheduler Name
    'execute-five-seconds': {
        # Task Name (Name Specified in Decorator)
        'task': 'run-chain',  
        # Schedule      
        'schedule': 5.0,
        # Function Arguments 
        'args': (
            'welcome',
            'Hey, how is it going?',
            'my name is Purple Alien',
            'I come from Andromeda'
        ) 
    }
}  
```

The output on Worker:

```terminal
[2022-09-28 08:41:31,066: INFO/MainProcess] Task run-chain[5c7ee128-9927-4d9c-a63c-33cd3882a6e0] received
[2022-09-28 08:41:31,071: INFO/ForkPoolWorker-4] Task run-chain[5c7ee128-9927-4d9c-a63c-33cd3882a6e0] succeeded in 0.0035799120014416985s: <AsyncResult: b7b00a07-4e50-441a-9bef-97ca06edf803>
[2022-09-28 08:41:31,071: INFO/MainProcess] Task state-name[9b313d40-317c-4558-90bc-3ff4a5ae725f] received
[2022-09-28 08:41:31,075: INFO/MainProcess] Task greeting[ac3381d1-660d-44a5-96d9-32716a2d48e2] received
[2022-09-28 08:41:31,075: INFO/ForkPoolWorker-4] Task state-name[9b313d40-317c-4558-90bc-3ff4a5ae725f] succeeded in 0.0028091490003134822s: 'welcome'
[2022-09-28 08:41:31,080: INFO/ForkPoolWorker-2] Task greeting[ac3381d1-660d-44a5-96d9-32716a2d48e2] succeeded in 0.003860558999804198s: '=== welcome: Hey, how is it going? ==='
[2022-09-28 08:41:31,080: INFO/MainProcess] Task get-identity[3d5215db-643d-48dc-8a78-67ab90db369f] received
[2022-09-28 08:41:31,085: INFO/MainProcess] Task get-location[b7b00a07-4e50-441a-9bef-97ca06edf803] received
[2022-09-28 08:41:31,086: INFO/ForkPoolWorker-4] Task get-identity[3d5215db-643d-48dc-8a78-67ab90db369f] succeeded in 0.0039819209996494465s: '=== === welcome: Hey, how is it going? ===: my name is Purple Alien ==='
[2022-09-28 08:41:31,088: INFO/ForkPoolWorker-2] Task get-location[b7b00a07-4e50-441a-9bef-97ca06edf803] succeeded in 0.0016402419987571193s: '=== === === welcome: Hey, how is it going? ===: my name is Purple Alien ===: I come from Andromeda ==='
```

The state is maintained but the whole output of the previous function is concatenated with the whole output of the next task. 

I want to have something like the following:

```terminal
=== welcome: Hey, how is it going? ===
=== welcome: My name is Purple alien ===' 
=== welcome: I come from Andromeda ===
```

Instead of:

```terminal
=== welcome: Hey, how is it going? ===
=== === welcome: Hey, how is it going? ===: my name is Purple Alien === 
=== === === welcome: Hey, how is it going? ===: my name is Purple Alien ===: I come from Andromeda ===
```
We can fix this by making the tasks immutable and independent so the result of the previous task will no be passed to the next task. Now, we can create a chain of independent tasks instead:

```python
@shared_task(name = 'run-chain')
def run_chain(state, message, identity, location):
    canvas = chain(
        get_state.si(state),
        get_greeting.si(state, message),
        get_identity.si(state, identity),
        get_location.si(state, location)
    ).apply_async()
    return canvas
```
The output in the worker:

```terminal
[2022-09-29 06:28:05,660: INFO/MainProcess] Task run-chain[8dd24c1a-265f-4ffb-a401-a547cec4c448] received
[2022-09-29 06:28:05,666: INFO/MainProcess] Task state-name[e00ac019-3ead-456a-a08b-495304af9768] received
[2022-09-29 06:28:05,666: INFO/ForkPoolWorker-4] Task run-chain[8dd24c1a-265f-4ffb-a401-a547cec4c448] succeeded in 0.004578891999699408s: <AsyncResult: 41ef7401-e306-49e0-8f3f-b798e090b018>
[2022-09-29 06:28:05,671: INFO/ForkPoolWorker-4] Task state-name[e00ac019-3ead-456a-a08b-495304af9768] succeeded in 0.0038825309984531486s: 'welcome'
[2022-09-29 06:28:05,672: INFO/MainProcess] Task greeting[9b8a70d7-77ec-40fe-8bbe-f7f57668345d] received
[2022-09-29 06:28:05,681: INFO/ForkPoolWorker-4] Task greeting[9b8a70d7-77ec-40fe-8bbe-f7f57668345d] succeeded in 0.004482243999518687s: '=== welcome: Hey, how is it going? ==='
[2022-09-29 06:28:05,686: INFO/MainProcess] Task get-identity[cf5c6936-b1c6-44da-a951-cf800ad981ca] received
[2022-09-29 06:28:05,692: INFO/MainProcess] Task get-location[41ef7401-e306-49e0-8f3f-b798e090b018] received
[2022-09-29 06:28:05,693: INFO/ForkPoolWorker-4] Task get-identity[cf5c6936-b1c6-44da-a951-cf800ad981ca] succeeded in 0.0054931009999563685s: '=== welcome: my name is Purple Alien ==='
[2022-09-29 06:28:05,697: INFO/ForkPoolWorker-4] Task get-location[41ef7401-e306-49e0-8f3f-b798e090b018] succeeded in 0.0018387599993729964s: '=== welcome: I come from Andromeda ==='
```
But, let's say we want to delay the chain once it received and different delays to every task in the chain. Here, the chain will take three seconds to run and each task in the chain will be delayed with the corresponding time:

```python
@shared_task(name = 'state-name')
def get_state(state):
    time.sleep(3)
    return state


@shared_task(name = 'greeting')
def get_greeting(state, message):
    time.sleep(3)
    output = '=== ' + state + ': ' + message + ' ==='
    return output


@shared_task(name = 'get-identity')
def get_identity(state, identity):
    time.sleep(5)
    output = '=== ' + state + ': '+ identity + ' ==='
    return output


@shared_task(name = 'get-location')
def get_location(state, location):
    time.sleep(2)
    output = '=== ' + state + ': ' + location + ' ==='
    return output


@shared_task(name = 'run-chain')
def run_chain(state, message, identity, location):
    canvas = chain(
        get_state.si(state),
        get_greeting.si(state, message),
        get_identity.si(state, identity),
        get_location.si(state, location)
    ).apply_async(countdown = 3) # delay the chain not every task in the chain
    return canvas
```

The process is running in the Celery worker:

```
[2022-09-29 07:54:17,031: INFO/MainProcess] Task state-name[73ab117a-4444-422e-a84b-ea7a8d4a0adb] received
[2022-09-29 07:54:17,032: INFO/ForkPoolWorker-4] Task run-chain[a4003962-1a71-4a67-a5a8-592dec059d06] succeeded in 0.0407028800000262s: <AsyncResult: 3bd37208-aae9-4260-8594-543f7e5d9ac3>
[2022-09-29 07:54:23,803: INFO/ForkPoolWorker-4] Task state-name[73ab117a-4444-422e-a84b-ea7a8d4a0adb] succeeded in 3.006613085000936s: 'welcome'
[2022-09-29 07:54:23,803: INFO/MainProcess] Task greeting[3dc93e37-a971-4ecd-9ea4-77073fd58a35] received
[2022-09-29 07:54:26,812: INFO/ForkPoolWorker-4] Task greeting[3dc93e37-a971-4ecd-9ea4-77073fd58a35] succeeded in 3.0065906279996852s: '=== welcome: Hey, how is it going? ==='
[2022-09-29 07:54:26,812: INFO/MainProcess] Task get-identity[e0e74c59-d185-44ab-b893-9f7e50f28af7] received
[2022-09-29 07:54:31,823: INFO/MainProcess] Task get-location[3bd37208-aae9-4260-8594-543f7e5d9ac3] received
[2022-09-29 07:54:31,825: INFO/ForkPoolWorker-4] Task get-identity[e0e74c59-d185-44ab-b893-9f7e50f28af7] succeeded in 5.0106186539997s: '=== welcome: my name is Purple Alien ==='
[2022-09-29 07:54:33,835: INFO/ForkPoolWorker-2] Task get-location[3bd37208-aae9-4260-8594-543f7e5d9ac3] succeeded in 2.0092069239999546s: '=== welcome: I come from Andromeda ==='
```