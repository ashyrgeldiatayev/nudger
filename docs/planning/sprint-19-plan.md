# Sprint Plan


## Sprint 19: August 1 - August 7
* [ ] Get code passing tests on a branch
* [ ] Remove delay between last message and buttons appearing
* [ ] Change message queue to use Celery for queueing/scheduling
  - the bot should send each part of the state individually in order (user-text, bot-text, triggers)
* [ ] Handle creating a unique room name for each user
  - make a view that automatically redirects to a hardcoded room
  - research hashing options (birthday problem)
  - build a function that makes a unique value for room name


## Done

### Sprint 18: July 25 - July 31
* [X] GR1-2: Overview of MOIA/Nudger (30 min) > Solve a problem together (30 min)
  - mention two problems to Rochdi (early) > work on these in meeting
* [X] G4-2.5: Selenium visits /chat-async/testroom/ and submits "English" and the new state is returned and the new state elements are not in the html that Selenium receives/picks up
* [X] G8-1.5: The user visits /chat-async/testroom/ and new text appears correctly, but when they refresh the page, the chatbot stops working (blank except the input field) 
  - need to store state/context in a line of Javascript
* [X] G2-2: Document edge cases
* [X] HG2-1: Do research on pure Django tests for channels (1 hour each)
  - Write most basic test possible for channels that doesn't require Selenium
* [X] G1.5-1.5: Upgrade/create Docstring about 10 important functions
  - """ Render view of index.html template for choose_room textbox """
* [X] G8: Delay messages so they come across at a more natural pace
  - 5- hard code a fixed # of seconds delay
  - 0.5- hard code a fixed # of seconds per char delay (0.1)
  - 0.5?- create kwarg with default value of 0.1 in function/class init
         * https://channels.readthedocs.io/en/stable/topics/consumers.html?highlight=scope#scope
         * https://channels.readthedocs.io/en/stable/topics/routing.html
  - (not get to) add to v4.yml default meta or input
  - read that during the processing of the yml and change kwarg value as you instantiate the consumer


### Sprint 17: July 18 - July 24
* [X] G2-3.5: Complete entire tutorial
* [X] G2-3: Incorporate the styling (change templates)
* [X] G6-5: Change the consumers (ie., populate more than message text)
* [X] G8- 9.5: Incrementally work towards building what is needed for Poly (from tutorial)
  - build out the json packet / make the response the same as the one responding to the channels
* [X] G2.5: Add Selenium tests
* [ ] Refine the analytics
