# Real feedback and thoughts on Poly 

## BB

```
Hey Rochdi,

Thanks for sharing! I took a look, and compared the two versions of Poly. I like that I can see where to click for the chatbot on the new version since you've added a background color. I do feel that the old version responds quicker, but other than that the styling and the flow of the new version is preferred.

Coming from someone who can be considered a "first time user", it would be great if there was a small info tip box that explained the purpose of the chatbot. Maybe this could appear when the user initially hovers over the bubble. I only say this because I was a bit confused as to what I needed to use the chatbot for, so a little more context would be great.

Hope this helps in some way!
```
## SS

```
I like in the old version the answers appear as soon as I respond. It looks more cluttered but it is more efficient.
```

classifying the user feedback into key concepts
summarize the feedbak 
structure the feedback into concise data
