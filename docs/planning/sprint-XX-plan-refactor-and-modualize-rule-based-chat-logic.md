# Sprint(s) for building ConversationalPipeline

The following rule-based chat engines have gotten bloated and contain a lot of redundant code.
Gradually (incrementally) refactor the chat_async app to install and import qary and use the preprocessing and loading functions from qary rather than copy-pasted code within nudger.
Delete as much as you can in nudger/data/v3.py

## Preprocessing/loading of yaml files

### Combine these files into one:

When you're done, only `qary/chat/v4.py` should be left:

- [qary/chat/v4.py](https://gitlab.com/tangibleai/qary/-/blob/main/src/qary/chat/v4.py)
- [qary/chat/v3.py](https://gitlab.com/tangibleai/qary/-/blob/main/src/qary/chat/v3.py)
- [nudger/data/v3.py](https://gitlab.com/tangibleai/nudger/-/blob/main/data/v3.py)
- [nudger/chat_async/load_yaml.py](https://gitlab.com/tangibleai/nudger/-/blob/main/chat_async/load_yaml.py)

### Suggested Tasks
1. add qary to dependencies (redeploy and run integration tests)
2. use one of qary.chat.v3 or .v4 functions/classes or methods within chat_async or delete some code in `nudger/data/v3.py`
3. deploy and test with nudger integration tests
4. fix any bugs 
5. improve the code style and docstrings
6. add unittests to the docstring (and make sure they run during deployment) if it makes sense
7. go back to step 2 and do more refactoring trying to 


## "Running" the chatbot rule logic

### Combine these functions into one:

When you're done there should be no redundant code in nudger or qary. There should be only one chat engine that is processing the chat logic and deciding what to say next. And it should be located in the qary.chat module:

- [nudger/chat_async/consumers.py](https://gitlab.com/tangibleai/nudger/-/blob/main/chat_async/consumers.py)
- [qary/chat/dialog.py](https://gitlab.com/tangibleai/qary/-/blob/main/src/qary/chat/dialog.py)

1. run qary tests
2. fix qary quiz bot bugs (v3.py, v4.py, dialog.py)
3. incrementally import and use the dialog.py functions/classes within nudger/chat_async
4. run nudger integration tests, fix nudger bugs, delete nudger code go back to 1 and repeat

