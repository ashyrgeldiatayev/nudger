# Ashyr Sprint 01
  Jan 27

## Tasks
* [X] Clone this repository
* [X] Find models.State , edit or create a docstring that explains what the class does
* [X] Find the function, similar to State.get.nextState , add or edit docstring
* [X] Visit qary.ai and explore the chatbot dialog tree
* [X] Find the mathQuiz.yml file,  add or edit one of the states
* [ ] Push your changes to GitLab and create merge request
* [X] Find and watch / learn Corey Schafer Django
