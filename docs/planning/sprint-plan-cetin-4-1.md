# sprint-3

### python

[X] cetin: merge consumers.py and models.py in `chat_async/` with same files in `quizbot/
[X] cetin: incorporate ConversationFlow.id or Settings.id into python quizbot logic
[X] cetin: create two different yaml files for two different math quizbots
[X] cetin: test two different quizbots at two different urls within same deployed webapp

### JS
- rochdi: show Cetin how to build a bundle.js
- cetin+rochdi: use qary logo in place of Poly parot when building bundle.js
