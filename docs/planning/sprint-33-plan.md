# Sprint 33 (Nov 04 - Nov 11)

* [x] R1-1: Create a md file that includes the MOIA report analysis notes of October
* [x] R0.1-R0.1: Share the file with Hobson and Maria for getting feedback
* [] R0.5: Send to the MOIA team the report analysis of October and all time with notes
* [] R1: Understand why the out-of-order bug has occurred
> Let's say the source of the OOO bug is the task queue 
* [] R2: Read/watch a tutorial to learn how to build a Celery task queue
* [] R0.1: Create a new branch called `fix-ooo-bug`
* [] R2: Resolve the MOIA Celery queue to fix the out-of-order bug
> The tasks above might change depending on the source of the bug
* [] R2: Take a tutorial to learn how to create an integration test
* [] R2: Create an integration test for MOIA to be tested automatically after every deployment 
* [] R0.5: Create a merge request that highlight the fixes and assign it to Hobson

## Done Sprint 32 (Oct 28 - Nov 04)

* [x] R1-0.5: Understand the Runtime error **Event loop is closed** 
* [x] R1-0.2: Fix the Runtime error **Event loop is closed**
* [x] R1-1: Run Flower dashboard locally to monitor Celery tasks
* [x] R2-2: Understand why the problem of repeated triggers and messages occurs
* [x] R1-1: Fix the problem of repeated triggers and messages
* [x] R2-1.5: Gnerate MOIA report analysis for October and all time
* [x] R0.5-0.2: Use the new web socket URL to general new links for the chat widget
* [x] R1-1: Take a quick tutorial on Django Extensions
* [x] R1-1: Take a quick tutorial on Django Debug Toolbar
* [x] R1-0.8: Diagnoze locally the out of order bug with Flower