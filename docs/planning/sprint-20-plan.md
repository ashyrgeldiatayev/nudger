# Sprint 20

## Aug 05 - Aug 12 (Rochdi)

* [x] R0.2-R0.1: Clone the [svelte-tawk](https://github.com/hellpirat/svelte-tawk-to-chat-widget) repository
* [x] R1-1: Adapt what is useful from the [tawk tutorial](https://raw.githubusercontent.com/codebubb/tawk-tutorial/main/src/Tawk.js) repo 
* ~~[x] H?: Grant Rochdi permission to push the repository to Tangible AI Gitlab~~
* ~~[x] R0.2-R0.1: Rename the remote origin to upstream~~
* ~~[x] R0.2-R0.1: Create a new remote called origin (git@gitlab.com:tangibleai/faceofqary)~~
* ~~[x] R0.5-R-1: Push the repo to Tangible AI (git push -u origin main)~~
* ~~[x] R0.2-0.1: Visit the repo and make it public~~
* ~~[x] R0.5-R0.2: Edit the README file by adding the project plan below~~
* ~~[x] R1-0.5: Build the app, and run it locally~~
> Plan changed here since the repo example doesn't provide an editable chat widget component
* [x] H?: Create a new repo named `faceofqary` and make it public
* [x] R0.1-0.1: Clone the `faceofqary` repository
* [x] R0.2-0.1: Edit the README file by adding the project plan below
* [x] R0.2-0.1: Create an empty Svelte project called **client**
* [x] R0.2-0.1: Add a `.gitignore` file to ignore node_modules and build files
* [x] R0.2-0.1: Push the empty svelte project to the `faceofqary` repository
* [x] R3-R2.5: Build a chat widget component using Svelte
> 
* [x] R1-1: Create a chat-window component that contains an empty text box and chat header
* [x] R1-1: Add a **send** that sends messages
* [x] R0.5-0.5: Up-scroll previous messages in the same window
* [x] R0.5-0.5: Make the **chat conversation** button initialize the chat (welcome message)
* [x] R0.5-1: Make the chat bullet echo what the user says in the chat 
* [x] R0.5-0.5: Delay the welcome message
* [x] R0.5-0.5: Edit the **start conversation** button when the chat starts
>
* [x] R0.5-0.5: Create an empty django project in the same repository
* [x] R1-0.5: Structure the repository using the monorepo multi-server approach
* [x] R1-0.5: Create two web services called **faceofqary-front** and **faceofqary-back**
* [x] R1-1: Deploy the application on Render one step at a time


## August 8 - August 14 (Greg)
* [ ] G4: Get start up delay with Daphne down (figure out what can be reduced)
* [ ] G6: Extract a value from user input (user_type)
  - extract user_type from states (i.e., is-this-your-first-no, is-this-your-first-yes)
  - save the entire message extracted value to the user context dictionary
  - ensure the saved value is preserved as the conversation is written
  - build a way to trigger this save function into yaml
  - make it more generalizable (Not this sprint)
* [ ] G8: Change message queue to use Celery for queueing/scheduling
  - the bot should send each part of the state individually in order (user-text, bot-text, triggers)
* [ ] G4: Handle creating a unique room name for each user
  - make a view that automatically redirects to a hardcoded room
  - research hashing options (birthday problem)
  - build a function that makes a unique value for room name
* [ ] G1: Create models_qary.py
  - copy over models.py into models_qary.py
  - consolidate NextStateReply function into models_qary.py

## Done

### Sprint 19: August 1 - August 7 (Greg)
* [X] G10: Review Websocket documentation to understand package problem and how websockets/celery/redis work together
* [X] G5: Get code passing tests on a branch
  - commented out consumer tests, because gitlab-ci fails without Redis deployed
* [X] G1.5: Set up a Redis server on render and connect nudger web app to it
* [X] G1: Migrate important poly_api code to chat_async
* [X] G4: Add doctests to chat_async
* [X] G0: Remove delay between last message and buttons appearing

## Project Plan

Build a prototype Svelte chatbot widget

Goal is to port the popout chatbot widget called Poly to Svelte. 
The typescript implementation is here: https://wespeaknyc.cityofnewyork.us/  (parrot icon in lower right)

https://gitlab.com/tangibleai/moia-poly-chatbot (source code of the chatbot widget)

A similar Django app (with CSS and HTML to get you started) is here:
https://www.qary.ai/poly/

And a Svelte app that talks to a Django REST API is available here:
https://playground.proai.org/

We have front-end and back-end source code for all of these apps that we will share with you.

And of course you can get started at https://svelte.dev/repl


## Useful Links

https://github.com/longstaff/chatbot

https://github.com/hellpirat/svelte-tawk-to-chat-widget 

https://gitlab.com/tangibleai/moia-poly-chatbot/

https://github.com/search?q=chat+widget+svelte
