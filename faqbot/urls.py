from django.urls import path
from faqbot.views import FaqCreateView, FaqListView, upload, DocumentListView, create_flow

urlpatterns = [
    path('', FaqListView.as_view(), name="faq-list"),
    path('new/', FaqCreateView.as_view(), name="faq-create"),
    path('upload/', upload, name='faq-upload'),
    path('documents/', DocumentListView.as_view(), name='faq-document-list'),
    path('documents/create-flow/', create_flow, name='faq-create-flow'),
]
