from django.contrib import admin
from faqbot.models import Faq, Document

# Register your models here.
admin.site.register(Faq)
admin.site.register(Document)
