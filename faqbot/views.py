from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.views.generic import ListView, CreateView
import urllib.request
from faqbot.forms import DocumentForm
from faqbot.models import Faq, Document
from nudger.settings import MEDIA_URL, MEDIA_ROOT, USE_S3
from quizbot.load_yaml import create_flow_data
import io


class FaqListView(LoginRequiredMixin, ListView):
    """Returns questions as list"""
    model = Faq
    template_name = 'faqbot/faq_list.html'
    context_object_name = 'faqs'
    ordering = ['-created_on']  # '-' to descending
    paginate_by = 10

    def get_queryset(self):
        """Overriding query method"""
        return Faq.objects.filter(user_id=self.request.user.id).order_by('-created_on')


class FaqCreateView(LoginRequiredMixin, CreateView):
    """Records questions to DB"""
    model = Faq
    fields = ['question']
    template_name = "faqbot/faq_create.html"
    DEFAULT_QUESTION = "Who are you?"
    DEFAULT_ANSWER = "I'm Qary!"

    def form_valid(self, form):
        question = form.cleaned_data.get("question")
        answer = self.DEFAULT_ANSWER if question == self.DEFAULT_QUESTION else "I don't know that!"
        form.instance.user_id = self.request.user
        form.instance.answer = answer
        form.save()
        return JsonResponse({"question": question, "answer": answer})


@staff_member_required
def upload(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('faq-document-list')
    else:
        form = DocumentForm()
    return render(request, 'faqbot/upload.html', {
        'form': form
    })


class DocumentListView(PermissionRequiredMixin, ListView):
    """Returns questions as list"""
    permission_required = 'is_staff'
    model = Document
    template_name = 'faqbot/document_list.html'
    context_object_name = 'documents'
    ordering = ['-created_on']  # '-' to descending
    paginate_by = 10

    def get_queryset(self):
        """Overriding query method"""
        return Document.objects.all().order_by('-created_on')


@staff_member_required
def create_flow(request):
    if USE_S3:
        url = f"{MEDIA_URL}{request.POST.get('path_')}"
        response = urllib.request.urlopen(url)
        io_wrapper = io.TextIOWrapper(response, encoding='utf-8')
        result = create_flow_data(io_wrapper)
    else:
        result = create_flow_data(f"{MEDIA_ROOT}/{request.POST.get('path_')}")
    if result:
        messages.info(request, result)
    return redirect('faq-document-list')
