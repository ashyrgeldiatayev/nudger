
terraform {
  backend "s3" {
    bucket         = "my-terraform-remote-s3-backend"
    key            = "nudger.my_remote_backend.tfstate"
    region         = "us-west-1"
    dynamodb_table = "nudger.s3-terraform-state-lock"
  }
}


resource "aws_dynamodb_table" "dynamodb-terraform-lock" {
  name           = "${local.table_name}"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }
}

locals {
  table_name = "${var.project_name}.s3-terraform-state-lock"
}
