
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}


# Configure the AWS Provider
provider "aws" {
  region     = var.region
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
}


resource "null_resource" "zip_nudger" {

  provisioner "local-exec" {
    command = <<EOT
      zip -r nudger.zip ../../nudger -x '/*.git/*' '/*venv/*'
    EOT
  }
}


resource "aws_instance" "my_instance_name" {
  # ami                    = "ami-01f87c43e618bf8f0"
  ami                    = data.aws_ami.ubuntu_ami.id # latest ubuntu
  instance_type          = var.instance_type
  key_name               = aws_key_pair.ssh_key.key_name
  vpc_security_group_ids = [aws_security_group.my_security_group.id]
  count                  = 2

  tags = {
    Name = element(var.name_tags, count.index)
  }

  # File Provisioner
  # The file provisioner is used to copy files or directories from the machine executing Terraform to the newly created resource.
  # https://www.terraform.io/language/resources/provisioners/file
  provisioner "file" {
    source      = "nudger.zip"
    destination = "nudger.zip"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = file(local.ssh_private_key)
      host        = self.public_ip
    }
  }

  # local-exec Provisioner
  # The local-exec provisioner invokes a local executable after a resource is created.
  # https://www.terraform.io/language/resources/provisioners/local-exec
  provisioner "local-exec" {
    on_failure = continue
    command    = <<EOT
      chmod +x scripts/configure_server.sh
      PUBLIC_IP=${self.public_ip} project_name=${var.project_name} scripts/configure_server.sh
    EOT
  }
}

output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.my_instance_name[*].id
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.my_instance_name[*].public_ip
}


resource "aws_key_pair" "ssh_key" {
  key_name   = local.key_name
  public_key = file(local.ssh_public_key)
}


# security_group dynamic blocks
# https://www.terraform.io/language/expressions/dynamic-blocks
# You can dynamically construct repeatable nested blocks
resource "aws_security_group" "my_security_group" {
  name        = "my_security_group"
  description = "Ingress for my_security_group"

  dynamic "ingress" {
    for_each = var.sg_ports
    iterator = port
    content {
      from_port   = port.value
      to_port     = port.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic "egress" {
    for_each = var.sg_ports
    content {
      from_port   = egress.value
      to_port     = egress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
}

# Data Source: ubuntu_ami
# Use this data source to get the ID of a registered AMI for use in other resources.
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami
data "aws_ami" "ubuntu_ami" {
  most_recent = true
  owners      = ["099720109477"] # Canonical


  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

output "ubuntu_name" {
  value = data.aws_ami.ubuntu_ami.name
}
