
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region     = var.region
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
}

# Provides a S3 bucket resource.
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket
resource "aws_s3_bucket" "my-terraform-remote-s3-backend" {
  bucket = "my-terraform-remote-s3-backend"
}

output "my-terraform-remote-s3-backend" {
  value = aws_s3_bucket.my-terraform-remote-s3-backend.bucket_domain_name
}

variable "region" {
  default = "us-west-1"
}

variable "AWS_ACCESS_KEY" {
  type        = string
  description = "This is an example input variable using env variables."
}

variable "AWS_SECRET_KEY" {
  type        = string
  description = "This is another example input variable using env variables."
}
