#!/bin/bash

mkdir -p ~/.ssh/aws/keys/
cd ~/.ssh/aws/keys/
ssh-keygen -P "" -t rsa -b 4096 -m pem -f ${project_name}-ssh-key
mv ${project_name}-ssh-key ${project_name}-ssh-key.pem
chmod 400 ${project_name}-ssh-key.pem
