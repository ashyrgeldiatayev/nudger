#!/usr/bin/env bash

source .env

pip install -r requirements.txt

python manage.py makemigrations
python manage.py migrate quizbot chat_async sms_nudger
python manage.py migrate
python manage.py migrate --run-syncdb

# python manage.py shell << EOF
# from quizbot.load_yaml import *
# create_states_and_messages()
# create_triggers()
# exit()
# EOF

# ./scripts/create_django_superuser.sh "$DJANGO_SUPERUSER_USERNAME" "$DJANGO_SUPERUSER_PASSWORD"

python manage.py shell <<EOF

print('Creating a superuser')

DJANGO_SUPERUSER_USERNAME = "$1"
DJANGO_SUPERUSER_PASSWORD = "$2"


from django.contrib.auth.models import User

User.objects.create_superuser(
    username = DJANGO_SUPERUSER_USERNAME,
    password = DJANGO_SUPERUSER_PASSWORD
)

print(f"len(password): len(DJANGO_SUPERUSER_PASSWORD)")

exit()

EOF


echo "Finished creating user '$DJANGO_SUPERUSER_USERNAME'."

python manage.py collectstatic --noinput -v 3


# # need to add the --password option with a custom management command
# # see: https://vuyisile.com/how-to-automate-creating-a-django-super-user/
# python manage.py createsuperuser --noinput \
#     --username "$DJANGO_SUPERUSER_USERNAME" \
#     --password "$DJANGO_SUPERUSER_PASSWORD" \
#     --email "$DJANGO_SUPERUSER_EMAIL"
