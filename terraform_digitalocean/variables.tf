# variables.tf

output "instance_id" {
  description = "ID of the digitalocean_droplet instance"
  value       = digitalocean_droplet.my-instance-name.id
}

output "ipv4_address" {
  description = "Public IP address of the digitalocean_droplet instance"
  value       = digitalocean_droplet.my-instance-name.ipv4_address
}


# in ~/.secrets add: export TF_VAR_<variable_name>="<value>"
variable "TERRAFORM_TOKEN" {}
