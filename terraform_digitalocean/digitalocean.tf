terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}


# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.TERRAFORM_TOKEN
}


# Create a new Web Droplet in the nyc2 region
resource "digitalocean_droplet" "my-instance-name" {
  image    = "ubuntu-20-04-x64"
  name     = "nudger-staging"
  region   = "nyc2"
  size     = "s-1vcpu-1gb"
  ssh_keys = [digitalocean_ssh_key.default.fingerprint]
}


# Create a new SSH key
resource "digitalocean_ssh_key" "default" {
  name       = "NUDGER_SSH_KEY"
  public_key = file("~/.ssh/digitalocean/keys/nudger-ssh-key.pub")
}
