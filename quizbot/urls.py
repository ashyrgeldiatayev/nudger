from django.urls import path

from . import views

urlpatterns = [
    path('api/', views.APINextStateReply.as_view(), name='quizbot-next-state'),
    path('', views.quiz, name='quiz'),
    path('<str:room_name>/', views.room, name='quizbot-room'),
]
