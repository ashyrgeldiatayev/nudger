import os
import json

from asgiref.sync import async_to_sync
from celery import shared_task
from channels.layers import get_channel_layer
from django.utils import timezone
from django_celery_beat.models import PeriodicTask, ClockedSchedule

from quizbot.models import InteractionData, State

os.environ['DJANGO_SETTINGS_MODULE'] = 'nudger.settings'


@shared_task(name="schedule_chat_message_2")
def schedule_chat_message_2(room_group_name, state_data, delay, interaction_data, conversation_flow_id):
    """ Schedules a one-time message to be sent based on the time and delay """
    # time = datetime.now()
    time = timezone.now()

    clocked, created = ClockedSchedule.objects.get_or_create(
        clocked_time=timezone.now() + timezone.timedelta(seconds=delay)
    )

    PeriodicTask.objects.create(
        clocked=clocked,
        name=f"Scheduled to send to {room_group_name} at {time}",
        task="send_out_chat_message_2",
        args=json.dumps([room_group_name, state_data, interaction_data, conversation_flow_id]),
        one_off=True,
    )


@shared_task(name="send_out_chat_message_2")
def send_out_chat_message_2(room_group_name, state_data, interaction_data, conversation_flow_id):
    """ Sends out an individual message to the channel group """
    print(conversation_flow_id)
    channel_layer = get_channel_layer()

    async_to_sync(channel_layer.group_send)(
        room_group_name,
        {
            'type': 'chat_message',
            'message': state_data
        }
    )

    InteractionData.objects.create(
        message_direction=interaction_data['message_direction'],
        message_text=interaction_data['message_text'],
        state=State.objects.get(
            state_name=interaction_data['state'],
            conversation_flow_id=conversation_flow_id),
        sender_type=interaction_data['sender_type'],
        user_id=interaction_data['user_id'],
    )
