from django.contrib import admin

from .models import (
    State,
    Message,
    Trigger,
    InteractionData,
    ConversationFlow,
    NudgerGlobalSettings,
)

# Register your models here.
admin.site.register(State)
admin.site.register(Message)
admin.site.register(Trigger)
admin.site.register(ConversationFlow)
admin.site.register(NudgerGlobalSettings)


class InteractionDataAdmin(admin.ModelAdmin):
    readonly_fields = ('send_time',)


admin.site.register(InteractionData, InteractionDataAdmin)
