import hashlib
import time
import uuid
from datetime import datetime

from django.shortcuts import render, redirect
from rest_framework.response import Response
from rest_framework.views import APIView

from nudger.settings import RENDER

from quizbot.models import State


def room(request, room_name):
    file_css = 'quizbot/quizbot_remote_bundle/quizbot.css' if RENDER else 'quizbot/mathbot_local_bundle/mathbot.css'
    file_js = 'quizbot/quizbot_remote_bundle/quizbot.js' if RENDER else 'quizbot/mathbot_local_bundle/mathbot.js'
    return render(request, 'quizbot/mathbot.html', {
        'room_name': room_name, 'file_css': file_css, 'file_js': file_js
    })


def quiz(request):
    hash = hashlib.sha1()
    hash.update(str(time.time()).encode('utf-8'))
    room_name = hash.hexdigest()[:7]

    return redirect('quizbot-room', room_name)


class APINextStateReply(APIView):
    """ Returns the next state based on the input

    Uses the State.get_next_state_from_database in models.py to find the next state information

    Use with /quiz/api

    JSON Input Example
    {
        'state_name': 'select-language',
        'user_text': 'English',
        'context': {'lang': 'en'}
    }

    Query Parameter Example
    /quiz/api/?state_name=selected-language-welcome&user_text=Ready
    """

    def get(self, request):
        data = request.data
        data.update(request.query_params.dict())
        time = datetime.now()
        message_id = uuid.uuid4()

        response = State.get_next_state_from_database(data)

        return Response({
            "state": response['state'],
            "messages": response['messages'],
            "triggers": response['triggers'],
            "context": response['context'],
            "message_id": message_id,
            "message_direction": "outbound",
            "sender_type": "chatbot",
            "user_id": "chat_j2Lk356",
            "bot_id": "Poly Chatbot",
            "channel": "WSNYC Website",
            "send_time": time

        })
