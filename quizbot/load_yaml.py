from .mathbot_v1 import coerce_dialog_tree_series
from quizbot.models import State, Message, Trigger, ConversationFlow

lang = "en"

langs = ["en", "es", "zh-hans", "zh-hant"]


def create_flow_data(file_path):
    DIALOG_TREE = coerce_dialog_tree_series(file_path)
    dialog_ = {node['name']: node for node in DIALOG_TREE}

    try:
        version = dialog_.get("start").get("version")
        bot_name = dialog_.get("start").get("bot_name")
    except AttributeError:
        return "Key, start, version, and bot_name might missing!"
    conversation_flow = ConversationFlow.objects.filter(name=bot_name, version=version)
    if conversation_flow: return "Conversation already exists!"

    conversation_flow_new = ConversationFlow(
        name=bot_name,
        version=version,
    )
    conversation_flow_new.save()

    for name in dialog_:
        state = State(
            state_name=name,
            conversation_flow=conversation_flow_new,
        )
        state.save()

        for lang in langs:
            # Makes an entry in the Message table for each English message of a State
            for sequence_number, message in enumerate(dialog_[name].get("actions", {}).get(lang, "")):
                message = Message(
                    state=state,
                    sequence_number=sequence_number,
                    bot_text=message,
                    lang=lang,
                )
                message.save()

    for name in dialog_:
        state = State.objects.get(
            state_name=name,
            conversation_flow=conversation_flow_new,
        )

        for lang in langs:
            # TODO: See if this try can be simplified to test whether there is a langauge represented and then take the stuff if it is or pass if it isn't

            try:
                functions_to_call = dict(dialog_[name].get("triggers", {}).get("functions", "").items())
            except:
                functions_to_call = None

            try:
                triggers = list(dialog_[name].get("buttons", {}).get(lang, "").items())
            except AttributeError:
                triggers = list(dialog_[name].get("buttons", {}).get(lang, ""))

            for intent_text, to_state_name in triggers:
                trigger = Trigger(
                    from_state=state,
                    to_state=State.objects.get(state_name=to_state_name, conversation_flow=conversation_flow_new),
                    intent_text=intent_text,
                    update_kwargs=functions_to_call,
                    lang=lang,
                )
                trigger.save()
    return "Conversation created!"
