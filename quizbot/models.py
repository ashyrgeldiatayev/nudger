from datetime import datetime
import markdown
import sentry_sdk
import uuid

from collections import OrderedDict
from django.db import models
from sentry_sdk import add_breadcrumb


START_STATE_NAME = 'start'


def report_through_sentry(label, issue, data):
    # msg_template = "{issue}- data: {data}"
    # msg = msg_template.format(issue=issue, data=data)
    sentry_sdk.capture_message(issue, label)


class NudgerGlobalSettings(models.Model):
    parameter_name = models.CharField(max_length=255, null=False, unique=True, blank=False)
    parameter_str_1 = models.CharField(max_length=255)
    parameter_int_1 = models.IntegerField(blank=True, null=True)
    parameter_int_2 = models.IntegerField(blank=True, null=True)
    parameter_bool_1 = models.BooleanField(blank=True, null=True)


# class Bot(models.Model):
#     name = models.CharField(max_length=100, null=False, blank=False)
#     flow = models.ForeignKey(ConversationFlow)


class ConversationFlow(models.Model):
    name = models.CharField(max_length=255, default='convoflow_created:' + datetime.now().isoformat(), blank=False, null=False)
    version = models.CharField(max_length=11, blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True, editable=False)


class State(models.Model):
    # Foreign key to itself

    lang_dict = {
        "English": 'en',
        "Spanish (Español)": 'es',
        "Chinese - Simplified (简体中文)": 'zh-hans',
        "Chinese - Traditional (繁體中文)": 'zh-hant'
    }

    next_states = models.ManyToManyField(
        "self",
        through="Trigger",
        through_fields=("from_state", "to_state")
    )
    state_name = models.CharField(
        null=False,
        max_length=255,
        help_text="The title the system uses to recognize the state"
    )
    update_args = models.JSONField(null=True)
    update_kwargs = models.JSONField(null=True)
    update_context = models.JSONField(
        null=True,
        help_text="May hold information such as gender, location, or history of previous exchanges"
    )
    level = models.IntegerField(
        null=True,
        help_text="An optional field that tells the depth of the action"
    )

    conversation_flow = models.ForeignKey(ConversationFlow, on_delete=models.CASCADE)

    @staticmethod
    def add_language_setting_to_context(data):
        """ Adds lang (language setting) when called from a state
            Only supposed to be used after the initial_state loads
        """

        user_text = data.get('user_text')
        lang_choice = State.lang_dict.get(user_text, 'en')

        data['context']['lang'] = lang_choice

        return data

    @staticmethod
    def create_initial_user_type_dict(triggers):
        """ Builds a dictionary of user_type roles from triggers in the target state """
        user_type_dict_English = {}
        user_type_dict_English_keys = []
        for trigger in triggers:
            if trigger.lang == 'en':
                user_type_dict_English[trigger.intent_text] = [trigger.intent_text]
                user_type_dict_English_keys.append(trigger.intent_text)
        return user_type_dict_English, user_type_dict_English_keys

    @staticmethod
    def update_user_type_dict(triggers, user_type_dict_English, user_type_dict_English_keys, lang_list):
        """ Takes an dict of user_types in English and adds corresponding entries for other languages """
        for lang in lang_list:
            tr_sublist = []
            for trigger in triggers:
                if trigger.lang == lang:
                    tr_sublist.append(trigger)
            for index in range(3):
                user_type_dict_English[user_type_dict_English_keys[index]].append(tr_sublist[index].intent_text)
        return user_type_dict_English

    @staticmethod
    def generate_user_type_dict():
        """ Builds a dictionary of user_types for the 'is-this-your-first-yes' state
        Used to set context dictionary's user_type
        """
        state = State.objects.filter(state_name='is-this-your-first-yes')[0]
        triggers = Trigger.objects.filter(from_state=state)

        user_type_dict_English, user_type_dict_English_keys = State.create_initial_user_type_dict(triggers)

        # lang_dict = self.send_language_dictionary(self)
        lang_list = State.lang_dict.values()

        user_type_dict = State.update_user_type_dict(
            triggers,
            user_type_dict_English,
            user_type_dict_English_keys,
            lang_list
        )
        return user_type_dict

    @staticmethod
    def add_user_type_to_context(data):
        """ Adds the user_type when called from a state
        'WSNYC Volunteer + Educator'
        'WSNYC Student'
        """

        user_type_dict = State.generate_user_type_dict()

        user_type = ''
        for val in user_type_dict.values():
            if data['user_text'] in val:
                user_type = list(user_type_dict.keys())[list(user_type_dict.values()).index(val)]

        data['context']['user_type'] = user_type
        return data

    @staticmethod
    def state_serializer(state):
        return {'state_name': state}

    @staticmethod
    def message_serializer(state, lang):
        messages = Message.objects.filter(state=state, lang=lang).values_list("sequence_number", "bot_text")

        message_serializer_list = []
        for message in messages:
            message_serializer_list.append(
                OrderedDict(
                    [
                        ('sequence_number', message[0]),
                        ('bot_text', markdown.markdown(
                            message[1], extensions=['attr_list']
                        )
                        )
                    ]
                )
            )
        return message_serializer_list

    @staticmethod
    def trigger_serializer(state, lang):
        triggers = Trigger.objects.filter(from_state=state, lang=lang).values_list("intent_text",
                                                                                   "to_state")
        trigger_serializer_list = []
        for trigger in triggers:
            state_result = State.objects.get(pk=trigger[1])
            trigger_serializer_list.append(
                OrderedDict(
                    [
                        ('intent_text', trigger[0]),
                        ('to_state', state_result.state_name)
                    ]
                )
            )
        return trigger_serializer_list

    @staticmethod
    def get_next_state_from_database(data, conversation_flow):
        """ input the current state (state_name) + message user selected (user_text)
        get_next_state_from_database returns a JSON object of the next state's data necessary to fill the frontend.
        """

        add_breadcrumb(
            category='Database Call Function',
            message='get_next_state_from_database',
            level='info'
        )

        # Set the state_name if value missing
        if "state_name" not in data or data['state_name'] == '':
            report_through_sentry('info', 'Missing state_name', data)

            data['state_name'] = 'select-language'
            data['user_text'] = data.get("user_text", '')

        # Set the user_text if value missing
        if "user_text" not in data or data['user_text'] == '':
            report_through_sentry('info', 'Missing user_text', data)

            data['user_text'] = ''

        # Set the context if value missing
        try:
            if data['context'] == '' or data['context'].get('lang') == '':
                data['context'] = {'lang': 'en'}
        except KeyError:
            data['context'] = {'lang': 'en'}

        # Test if the state exists in the database
        try:
            initial_state = State.objects.get(state_name=data['state_name'], conversation_flow=conversation_flow)
        except State.DoesNotExist as err:
            sentry_sdk.capture_exception(err)

            content = {'error_message': 'No matching state'}

            return content

        # Handle the default starting state - 'select-language'
        if data['state_name'] == START_STATE_NAME and data['user_text'] == '':
            state_serializer = State.state_serializer(initial_state.state_name)
            message_serializer = State.message_serializer(initial_state, data['context']['lang'])
            trigger_serializer = State.trigger_serializer(initial_state, data['context']['lang'])

            return {
                "state": state_serializer,
                "messages": message_serializer,
                "triggers": trigger_serializer,
                "context": {'lang': 'en'}
            }

        # Get available states for the initial_state
        # Allow matching of any response in any language
        triggers = Trigger.objects.filter(from_state=initial_state)

        # Test if the user_text is an option in the state
        user_text_test = False
        next_state = ''
        if data['user_text'] != '':
            for trigger in triggers:
                if trigger.intent_text.lower() == data['user_text'].lower():
                    user_text_test = True
                    next_state = trigger.to_state
                    break
            if user_text_test is False:
                report_through_sentry('info', 'Unacceptable response', data)

                content = {'error_message': 'Response is not an option', 'data': data}
                return content

            # Test for and execute functions associated with a state
            state_test = State.objects.filter(state_name=data['state_name'], conversation_flow=conversation_flow)
            trig = Trigger.objects.filter(from_state=state_test[0], intent_text=data['user_text'])

            try:
                if 'context_functions' in trig[0].update_kwargs.keys():
                    for func_name in trig[0].update_kwargs['context_functions']:
                        output = getattr(State, func_name, lambda: 'Not found')(data)
                        data['context'] = output['context']
            except AttributeError as err:
                print(err)

            # Having a user_text and an initial_state that's not 'select-language' should go to next_state
        else:
            # Not select-language and empty user_text should fail
            report_through_sentry('info', 'Missing required response', data)

            content = {"error_message": "User message is required", "data": data}
            return content

        # Build the response with the data for the next state
        state_serializer = State.state_serializer(next_state.state_name)
        message_serializer = State.message_serializer(next_state, data['context']['lang'])
        trigger_serializer = State.trigger_serializer(next_state, data['context']['lang'])

        return {
            "state": state_serializer,
            "messages": message_serializer,
            "triggers": trigger_serializer,
            "context": data['context']
        }


LANGUAGE_CHOICES = [
    ('en', 'en'),
    ('es', 'es'),
    ('zh-hans', 'zh-hans'),
    ('zh-hant', 'zh-hant')
]


class Message(models.Model):
    """ A table for messages the bot can send.
    Each row contains one message written in one language.
    """
    sequence_number = models.IntegerField(blank=False, null=False)
    state = models.ForeignKey(
        State,
        null=False,
        blank=False,
        on_delete=models.CASCADE
    )
    bot_text = models.TextField(
        null=False,
        help_text="The message of the state written in English"
    )
    lang = models.CharField(choices=LANGUAGE_CHOICES, default='en', max_length=100)


class Trigger(models.Model):
    """ A table of supported user inputs that connect two states.
    Users can enter the intent_text by button click or text input.
    Each row is one trigger option written in one language.
    """
    # Foreign Keys
    from_state = models.ForeignKey(
        State,
        related_name="rel_from_state",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    to_state = models.ForeignKey(
        State,
        related_name="rel_to_state",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    intent_text = models.CharField(
        null=False,
        max_length=255,
        help_text="The trigger message"
    )
    update_args = models.JSONField(null=True)
    update_kwargs = models.JSONField(null=True)
    lang = models.CharField(
        choices=LANGUAGE_CHOICES,
        default='en',
        max_length=100
    )


MESSAGE_DIRECTION = [
    ('inbound', 'inbound'),
    ('outbound', 'outbound'),
]

SENDER_TYPE = [
    ('user', 'user'),
    ('chatbot', 'chatbot'),
    ('human agent', 'human agent'),
]


class InteractionData(models.Model):
    """ A table that captures data each time a message is sent """
    message_id = models.UUIDField(
        default=uuid.uuid4,
        unique=True,
        help_text="A unique id assigned to each message that is sent out"
    )
    message_direction = models.CharField(
        choices=MESSAGE_DIRECTION,
        default='en',
        max_length=100
    )
    message_text = models.TextField(
        null=False,
        help_text="The message of the state"
    )
    send_time = models.DateTimeField(
        auto_now_add=True,
        help_text="The time that the message was sent (as opposed to scheduled)"
    )
    state = models.ForeignKey(
        State,
        null=True,
        max_length=255,
        on_delete=models.SET_NULL,
        help_text="The state of the chatbot when a message is sent"
    )
    sender_type = models.CharField(
        choices=SENDER_TYPE,
        max_length=20
    )
    user_id = models.CharField(
        null=True,
        max_length=255,
        help_text="A id for an anonymous user for one session"
    )
    bot_id = models.CharField(
        default="Poly Chatbot",
        max_length=255,
        help_text="The name or id of the chatbot being interacted with"
    )
    channel = models.CharField(
        default="WSNYC Website",
        max_length=128,
        help_text="The platform the user interacted with, such as WSNYC website"
    )

    """
    TODO Questions:
    * Should I incorporate status messages (like errors into this)?
    * For the state, should I return the state object or just the name
    """
