from datetime import timezone, timedelta

from django.utils import timezone
from django.test import TestCase
from django.contrib.auth.models import User
from faqbot.models import Faq
from django.urls import reverse


class FaqModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        User.objects.create_user(username="test_user", password="Testing321", email="test_user@example.com")
        user = User.objects.first()
        Faq.objects.create(user_id=user, question="Who are you?", answer="I am Qary!")

    def test_user_id_label(self):
        faq = Faq.objects.latest("created_on")
        field_label = faq._meta.get_field("user_id").verbose_name
        self.assertEqual(field_label, "user id")

    def test_question_label(self):
        faq = Faq.objects.latest("created_on")
        field_label = faq._meta.get_field("question").verbose_name
        self.assertEqual(field_label, "question")

    def test_answer_label(self):
        faq = Faq.objects.latest("created_on")
        field_label = faq._meta.get_field("answer").verbose_name
        self.assertEqual(field_label, "answer")

    def test_created_on_label(self):
        faq = Faq.objects.latest("created_on")
        field_label = faq._meta.get_field("created_on").verbose_name
        self.assertEqual(field_label, "created on")


class FaqListViewTest(TestCase):
    def setUp(self):
        # Create two users
        test_user1 = User.objects.create_user(username="test_user1", password="Testing321",
                                              email="test_user1@example.com")
        test_user2 = User.objects.create_user(username="test_user2", password="Testing321",
                                              email="test_user2@example.com")

        # Create 30 Instance objects
        number_of_faqs = 30
        for faq in range(number_of_faqs):
            created_on = timezone.localtime() + timedelta(days=faq % 5)
            user = test_user1 if faq % 2 else test_user2
            Faq.objects.create(
                user_id=user,
                created_on=created_on,
                question="Who are you?" if faq % 2 else "Is Mars bigger than the Moon?",
                answer="I'm Qary!" if faq % 2 else "I don't know that",
            )

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('faq-list'))
        self.assertRedirects(response, '/login/?next=/faq/')

    def test_logged_in_uses_correct_template(self):
        login = self.client.login(username='test_user1', password='Testing321')
        response = self.client.get(reverse('faq-list'))

        # Check our user is logged in
        self.assertEqual(str(response.context['user']), 'test_user1')
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

        # Check we used correct template
        self.assertTemplateUsed(response, 'faqbot/faq_list.html')

    def test_only_user_questions_in_list(self):
        login = self.client.login(username='test_user1', password='Testing321')
        response = self.client.get(reverse('faq-list'))

        # Check our user is logged in
        self.assertEqual(str(response.context['user']), 'test_user1')
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)
        # Confirm all faqs belong to test_user1
        for faq in response.context["object_list"]:
            self.assertEqual(faq.user_id.username, "test_user1")

    def test_pages_ordered_by_newest(self):
        login = self.client.login(username='test_user1', password='Testing321')
        response = self.client.get(reverse('faq-list'))

        # Check our user is logged in
        self.assertEqual(str(response.context['user']), 'test_user1')
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

        # Confirm that 30 of the items, only 10 are displayed due to pagination.
        self.assertEqual(len(response.context['object_list']), 10)

        last_date = float('inf')
        for faq in response.context["object_list"]:
            if last_date == float('inf'):
                last_date = faq.created_on
            else:
                self.assertTrue(last_date >= faq.created_on)
                last_date = faq.created_on


class FaqCreateViewTest(TestCase):
    def setUp(self):
        User.objects.create_user(username="test_user1", password="Testing321", email="test_user@example.com")
        self.client.login(username='test_user1', password='Testing321')

    def test_new_faq_post(self):
        response = self.client.post('/faq/new/', {'question': "Who are you?"})
        self.assertEqual(
            response.json(), {'question': 'Who are you?', 'answer': "I'm Qary!"}
        )
