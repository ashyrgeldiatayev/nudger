[![CICD Pipeline](https://gitlab.com/tangibleai/nudger/badges/main/pipeline.svg)](https://gitlab.com/tangibleai/nudger/badges/main/pipeline.svg)



# nudger
chatbot improves the educational experience for students by allowing students to recieve email and SMS notifications for important tasks, grades and milestones throughout the semester.


## Installation

Clone the repository, install requirements, and run the Django development web server:

```bash
git clone git@github.com:tangibleai/nudger.git
cd nudger
conda create -y -n nudgerenv 'python=3.8.8'
conda activate nudgerenv
conda env update -n nudgerenv -f environment.yml
```

## Run dev server

python manage.py runserver

## Production

Three services/servers on render.com:

1. nudger-main: django app (daphne async web server) `daphne -b 0.0.0.0 nudger.asgi:application`
2. nudger-celery-main render server: start-celery.sh (with a copy of django app to be able to run django tasks)
3. nudger-redis: RAM database for django to communicate tasks to celery server
