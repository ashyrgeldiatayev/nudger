import os

from asgiref.sync import async_to_sync
from celery import shared_task
from channels.layers import get_channel_layer
# from datetime import datetime, timedelta
from django.utils import timezone
from chat_async.consumers import *
from django_celery_beat.models import PeriodicTask, ClockedSchedule

from .models import InteractionData, State



os.environ['DJANGO_SETTINGS_MODULE'] = 'nudger.settings'


@shared_task(name="schedule_chat_message")
def schedule_chat_message(room_group_name, state_data, delay, interaction_data):
    """ Schedules a one-time message to be sent based on the time and delay """
    #time = datetime.now()
    time = timezone.now()

    clocked, created = ClockedSchedule.objects.get_or_create(
        #clocked_time=datetime.now() + timedelta(seconds=delay)
        clocked_time=timezone.now() + timezone.timedelta(seconds=delay)
    )

    PeriodicTask.objects.create(
        clocked=clocked,
        name=f"Scheduled to send to {room_group_name} at {time}",
        task="send_out_chat_message",
        args=json.dumps([room_group_name, state_data, interaction_data]),
        one_off=True,
    )


@shared_task(name="send_out_chat_message")
def send_out_chat_message(room_group_name, state_data, interaction_data):
    """ Sends out an individual message to the channel group """
    channel_layer = get_channel_layer()

    async_to_sync(channel_layer.group_send)(
        room_group_name,
        {
            'type': 'chat_message',
            'message': state_data
        }
    )

    InteractionData.objects.create(
        message_direction=interaction_data['message_direction'],
        message_text=interaction_data['message_text'],
        state=State.objects.get(state_name=interaction_data['state']),
        sender_type=interaction_data['sender_type'],
        user_id=interaction_data['user_id'],
    )







# from asgiref.sync import async_to_sync
# from celery import shared_task
# from channels.layers import get_channel_layer
# from datetime import datetime, timedelta
# ​
# from chat_async.consumers import *
# from django_celery_beat.models import PeriodicTask, ClockedSchedule
# ​
# from .models import InteractionData, State
# ​
# ​
# # NOTE: Celery beat (ie., this task) is probably not necessary anymore
# # @shared_task(name="schedule_chat_message")
# # def schedule_chat_message(room_group_name, state_data, delay, interaction_data):
# #     """ Schedules a one-time message to be sent based on the time and delay """
# #     time = datetime.now()
# ​
# #     clocked, created = ClockedSchedule.objects.get_or_create(
# #         clocked_time=datetime.now() + timedelta(seconds=delay)
# #     )
# ​
# #     PeriodicTask.objects.create(
# #         clocked=clocked,
# #         name=f"Scheduled to send to {room_group_name} at {time}",
# #         task="send_out_chat_message",
# #         args=json.dumps([room_group_name, state_data, interaction_data]),
# #         one_off=True,
# #     )
# ​
# from celery import chain
# from celery import group
# ​
# @shared_task
# def send_user_message(state_obj):
#     # get the trigger the user selected
#     # Send to the channel
# ​
# @shared_task
# def send_chat_message(state_obj):
#     # get the chat_message
#     # Send to the channel
# ​
# @shared_task
# def send_triggers(state_obj):
#     # get object of triggers
#     # Send to the channel
# ​
# @shared_task
# def send_chat_state(state_object):
#     """  Ideas
#     There might be two approaches - chain and group.
# ​
#     1) Chain - result is automatically used as the first argument to the next task (cumulative)
#     2) Group - each task executes and gets a result.  Results are not cumulative.
# ​
#     Overall process
#     1) 'consumers.py' calls 'send_chat_state' task
#     2) 'send_chat_state' builds a chain of 'send_user_message','send_chat_message','send_triggers'
#     3) tasks in chain execute
# ​
#     Delay
#     .apply_async(countdown=3) can implement the delay.  Will need to think about how to dynamically set the countdown.  (Probably just hardcode it first)
# ​
#     Chat Message (bot > user)
#     The send_chat_message task will need to run for each message the bot sends.  Need to think about how to dynamically build a chain.
#     """
# ​
#     # Each task should return the state object, because it will be piped into the next task
#     res_chain = chain(
#         send_user_message.s().apply_async(countdown=3),
#         send_chat_message.s().apply_async(countdown=3),
#         send_triggers.s().apply_async(countdown=3)
#     )
# ​
#     # Each task will implement one after the other in order (error handling seems complicated)
#     # A bit unclear whether they happen in sequence or in parallel
#     res_group = group(
#         send_user_message.s().apply_async(countdown=3),
#         send_chat_message.s().apply_async(countdown=3),
#         send_triggers.s().apply_async(countdown=3)        
#     )
# ​
#     res_chain.get()
#     res_group.get()
# ​
# ​
# # NOTE: This task will probably not be necessary if you remake it with the above notes, but elements of this will be useful.
# @shared_task(name="send_out_chat_message")
# def send_out_chat_message(room_group_name, state_data, interaction_data):
#     """ Sends out an individual message to the channel group """
#     channel_layer = get_channel_layer()
# ​
#     async_to_sync(channel_layer.group_send)(
#         room_group_name,
#         {
#             'type': 'chat_message',
#             'message': state_data
#         }
#     )
# ​
#     InteractionData.objects.create(
#         message_direction=interaction_data['message_direction'],
#         message_text=interaction_data['message_text'],
#         state=State.objects.get(state_name=interaction_data['state']),
#         sender_type=interaction_data['sender_type'],
#         user_id=interaction_data['user_id'],
#     )