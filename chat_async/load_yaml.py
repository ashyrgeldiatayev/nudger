# import yaml
from chat_async.v3 import DIALOG_TREE_DICT

from chat_async.models import State, Message, Trigger

lang = "en"

langs = ["en", "es", "zh-hans", "zh-hant"]

# State and Message Creation


def create_states_and_messages(dialog=DIALOG_TREE_DICT):
    dialog = DIALOG_TREE_DICT
    for name in dialog:
        s, created = State.objects.get_or_create(
            state_name=name,
        )
        if created:
            s.save()

            for lang in langs:

                # Makes an entry in the Message table for each English message of a State
                for sequence_number, message in enumerate(dialog[name].get("actions", {}).get(lang, "")):
                    m, created_m = Message.objects.get_or_create(
                        state=s,
                        sequence_number=sequence_number,
                        bot_text=message,
                        lang=lang
                    )
                    if created_m:
                        pass
                        # print(f"Created '{name}': {m.lang} - {sequence_number}  ")
        # print(name, s)


# Trigger Creation
# Problem: some buttons don't have certain language options
def create_triggers(dialog=DIALOG_TREE_DICT):
    for name in dialog:
        s = State.objects.get(state_name=name)
        for lang in langs:
            # TODO: See if this try can be simplified to test whether there is a langauge represented and then take the stuff if it is or pass if it isn't

            try:
                functions_to_call = dict(DIALOG_TREE_DICT[name].get("triggers", {}).get("functions", "").items())
            except:
                functions_to_call = None

            try:
                triggers = list(DIALOG_TREE_DICT[name].get("buttons", {}).get(lang, "").items())
            except AttributeError:
                triggers = list(DIALOG_TREE_DICT[name].get("buttons", {}).get(lang, ""))

            for intent_text, to_state_name in triggers:
                trigger, was_created = Trigger.objects.get_or_create(
                    from_state=s,
                    to_state=State.objects.get(state_name=to_state_name),
                    intent_text=intent_text,
                    update_kwargs=functions_to_call,
                    lang=lang
                )
                # print(f"{trigger} - {was_created} - {lang}")
                # if functions_to_call != None:
                #     print(f"{trigger} - {was_created}")


if __name__ == "__main__":
    create_states_and_messages()
    create_triggers()
